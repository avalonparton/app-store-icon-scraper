# App Store Icon Scraper

Grabs icons from Apple's App Store for making iOS themes.

# Usage

Run `scrape_icons.py` with Python3 with app names as the arguments.

```
python3 scrape_icons.py "discord" "apollo" 
 App: discord
 -- Search URL https://itunes.apple.com/search?limit=1&media=software&term=discord
 -- Icon URL: https://is3-ssl.mzstatic.com/image/thumb/Purple113/v4/58/89/18/5889183b-2a36-f645-782f-c72a18df22be/source/512x512bb.jpg
 -- BundleId: com.hammerandchisel.discord
 -- Saved icon as icons/com.hammerandchisel.discord-large.png
 App: apollo
 -- Search URL https://itunes.apple.com/search?limit=1&media=software&term=apollo
 -- Icon URL: https://is3-ssl.mzstatic.com/image/thumb/Purple113/v4/1c/6c/38/1c6c382d-e5a1-5035-bdeb-2773dcb350de/source/512x512bb.jpg
 -- BundleId: com.christianselig.Apollo
 -- Saved icon as icons/com.christianselig.Apollo-large.png
```