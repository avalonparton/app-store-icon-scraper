import requests
import sys
import shutil
import os
from stock_apps import stock_apps
from PIL import Image

# Set request headers
headers = requests.utils.default_headers()
headers.update({ 'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0'})

if len(sys.argv) == 1:
    print(f"Usage: {sys.argv[0]} [list of app names]")

def get_data(app):
    """ Gets the BundleId and app icon from the App Store. """
    # Check if this is a stock app
    
    stock_app = stock_apps.get(app)
    if stock_app is not None:
        print(" -- This is a stock app!")
        return stock_app[0], stock_app[1]

    url = f"https://itunes.apple.com/search?limit=1&media=software&term={app}"
    print(f" -- Search URL {url}")
    req = requests.get(url, headers)
    results = req.json().get('results')

    # Make sure we get a result
    if results is None or len(results) < 1:
        print("Failed to find app in App Store!")
        sys.exit(1)

    # Extract the data from the results
    bundle_id = results[0].get('bundleId')
    icon_url = results[0].get('artworkUrl512')

    return bundle_id, icon_url

# Search for each app in the App Store
for app in sys.argv[1:]:
    print(f" App: {app}")
    bundle_id, icon_url = get_data(app)
    icon_dir = 'icons'
    icon_filename = f"{icon_dir}/{bundle_id}.png"
    print(f" -- Icon URL: {icon_url}")
    print(f" -- BundleId: {bundle_id}")

    # Create the output folder
    if not os.path.isdir(icon_dir):
        try:
            os.mkdir(icon_dir)
        except:
            print(f"Failed to create icon directory: {icon_dir}")
            sys.exit(1)


    # Download the image
    response = requests.get(icon_url, stream=True)
    with open(icon_filename, 'wb') as out_file:
        shutil.copyfileobj(response.raw, out_file)
    print(f" -- Saved icon as {icon_filename}")

    # Make sure it's a png
    im = Image.open(icon_filename)
    im.save(icon_filename, "PNG")

    